<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/*
 * Plugin Name: AGV WooCommerce Saga
 * Plugin URI: https://www.agvision.ro/ro/plugin-export-si-facturare-woocommerce-saga/
 * Description: Această extensie facilitează exportul facturilor în programul de contabilitate Saga Soft.
 * Version: 0.9.1
 * Author: WooCommerce
 * Author URI: http://woocommerce.com/
 * Developer: AGVision Software
 * Developer URI: http://www.agvision.ro/
 * Text Domain: agv-woocommerce-saga
 * Copyright: © 2009-2015 WooCommerce.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

include_once(plugin_dir_path(__FILE__) . '/includes/classes/agv-saga.php');
include_once(plugin_dir_path(__FILE__) . '/includes/classes/agv-saga-api.php');

/**
 * Check if WooCommerce is active.
 **/
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    register_activation_hook(__FILE__, 'agv_saga_install');
    
    /**
     * Register new client to API. Gets called on plugin installation hook.
     *
     * @return void
     */
    function agv_saga_install()
    {
        $agvSaga = new agvSaga;

        if (get_option('agv_saga_registered') != 1) {
            // Register shop
            $data = array();
            $data['platform']         = 1;
            $data['key']              = $agvSaga->generateKey(10);
            $data['version']          = get_bloginfo('version');
            $data['module_version']   = $agvSaga->version;
            $data['config_name']      = get_bloginfo('name');
            $data['config_email']     = get_bloginfo('admin_email');

            $agvSagaApi = new agvSagaApi;
            $register   = $agvSagaApi->call('register', $data);

            if ($register['httpCode'] === 201) {
                update_option('agv_saga_registered', 1);
                update_option('agv_saga_key', $data['key']);
            } elseif ($register['httpCode'] === 409) {
                // Shop already created
            }
        }
    }
    /**
     * Add Export Saga action to Orders page.
     *
     * @param array $actions
     * @param Order $order
     * @return array
     */
    function agv_saga_order_action($actions, $order)
    {
        $newActions = [];

        $newActions['myLink'] = array(
            'url'    => plugin_dir_url(__FILE__) . 'export.php?orderId=' . $order->id,
            'name'   => 'Export Saga',
            'action' => 'export-saga'
        );

        return array_merge($newActions, $actions);
    }
    //add_filter('woocommerce_admin_order_actions', 'agv_saga_order_action', 10, 2);

    /**
    * Add Export Saga order bulk action to Orders page.
    *
    * @param array $actions
    * @return array
    */
    function agv_saga_bulk_actions($actions)
    {
        $newActions                = [];
        $newActions['export-saga'] = 'Export Saga';

        return array_merge($newActions, $actions);
    }
    add_filter('bulk_actions-edit-shop_order', 'agv_saga_bulk_actions');

    /**
     * Handle shop order export bulk action.
     *
     * @param  string $redirectTo  URL to redirect to.
     * @param  string $action      Action name.
     * @param  array  $ids         List of ids.
     * @return string
    */
    function agv_saga_handle_bulk_action($redirectTo, $action, $ids)
    {
        // If action is to export Saga then download XML file
        if ($action === 'export-saga') {
            $saga     = new AgvSaga;
            $xml      = $saga->getXml($ids);

            $cui      = get_option('agv_saga_cui');
            $series   = get_option('agv_saga_serie');
            $date     = date('d.m.Y', time());
            $filename = "filename=F_{$cui}_{$series}_00001_{$date}.xml";

            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);

            echo $xml;
        }
    }
    add_filter('handle_bulk_actions-edit-shop_order', 'agv_saga_handle_bulk_action', 10, 3);

    /**
     * Create the section beneath the products tab.
     **/
    function agv_saga_add_section($sections) {
        $sections['agv-saga'] = __('AGV Saga', 'agv-saga');
        return $sections;
    }
    add_filter('woocommerce_get_sections_products', 'agv_saga_add_section');

    /**
     * Add settings to the AGV Saga section.
     */
    function agv_saga_all_settings($settings, $current_section) {
        /**
         * Check if current section is what we want.
         **/
        if ($current_section == 'agv-saga') {
            $settings_agv_saga = [];
            // Add Title to the Settings
            $settings_agv_saga[] = array('name' => __('AGV Saga', 'agv-saga' ), 'type' => 'title', 'desc' => __('Configurați aici setările pentru export-ul facturilor în format Saga.', 'agv-saga'), 'id' => 'agv-saga');

            $settings_agv_saga[] = array(
                'name'     => __('CUI', 'agv-saga'),
                'default'  => get_option('agv_saga_cui'),
                //'desc_tip' => __('CUI-ul societății exact cum este setat în Saga.', 'agv-saga'),
                'id'       => 'agv_saga_cui',
                'type'     => 'text',
                'desc'     => __('CUI-ul societății exact cum este setat în Saga', 'agv-saga'),
            );

            $settings_agv_saga[] = array(
                'name'     => __('Gestiune', 'agv-saga'),
                'default'  => get_option('agv_saga_gestiune'),
                //'desc_tip' => __('Numele gestiunii exact cum este numită în Saga.', 'agv-saga'),
                'id'       => 'agv_saga_gestiune',
                'type'     => 'text',
                'desc'     => __('Numele gestiunii exact cum este numită în Saga', 'agv-saga'),
            );

            $settings_agv_saga[] = array(
                'name'     => __('Serie factură', 'agv-saga'),
                'default'  => get_option('agv_saga_serie'),
                //'desc_tip' => __('Seria facturilor exact cum este setată în Saga', 'agv-saga'),
                'id'       => 'agv_saga_serie',
                'type'     => 'text',
                'desc'     => __('Seria (fără număr) facturilor exact cum este setată în Saga', 'agv-saga'),
            );

            $settings_agv_saga[] = array(
                'name'     => __('Cod livrare', 'agv-saga'),
                'default'  => get_option('agv_saga_livrare'),
                //'desc_tip' => __('Codul metodei de livrare cum este setat în Saga.', 'agv-saga'),
                'id'       => 'agv_saga_livrare',
                'type'     => 'text',
                'desc'     => __('Codul metodei de livrare cum este setat în Saga', 'agv-saga'),
            );

            $settings_agv_saga[] = array(
                'name'     => __('Cod reducere', 'agv-saga'),
                'default'  => get_option('agv_saga_reducere'),
                //'desc_tip' => __('Codul pentru reduceri cum este setat în Saga.', 'agv-saga'),
                'id'       => 'agv_saga_reducere',
                'type'     => 'text',
                'desc'     => __('Codul pentru reduceri cum este setat în Saga', 'agv-saga'),
            );
            
            $settings_agv_saga[] = array('type' => 'sectionend', 'id' => 'agv-saga');

            return $settings_agv_saga;
        } else {
            return $settings;
        }
    }
    add_filter('woocommerce_get_settings_products', 'agv_saga_all_settings', 10, 2);
}
