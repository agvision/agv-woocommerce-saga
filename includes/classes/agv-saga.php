<?php

class AgvSaga
{
    public $version = '0.9.1';

    /**
     * Parse orders given and return XML.
     * @param  array $orderIds    IDs of orders to be exported
     * @return string             XML format of Saga orders
     */
    public function getXML(array $orderIds)
    {
        $data = [
            'key'    => get_option('agv_saga_key'),
            'type'   => 'export',
            'orders' => [],
        ];
        $wcpdf = false;

        foreach ($orderIds as $orderId) {
            $order         = wc_get_order($orderId);
            $products      = $order->get_items();
            $productsArray = [];
            // Check if WCPDF plugin is used
            
            if (!$wcpdf) {
                foreach ($order->meta_data as $meta) {
                    if ($meta->key === '_wcpdf_invoice_number') {
                        $wcpdf = true;
                    }
                }
            }

            if (!$wcpdf) {
                echo 'AGV Saga suportă exportul facturilor generate doar cu plugin-ul WooCommerce PDF Invoices & Packing Slips.';
                exit;
            }

            foreach ($products as $product) {
                $productsArray[] = [
                    'product_id' => $product['product_id'],
                    'model'      => (new WC_Product($product['product_id']))->get_sku(),
                    'name'       => $product['name'],
                    'quantity'   => $product['quantity'],
                    'subtotal'   => $product['subtotal'],
                    'total'      => $product['total'],
                    'total_tax'  => $product['total_tax']
                ];
            }

            // Append more relavant order data
            $data['orders'][] = [
                'agv_saga' => [
                    'cui'      => get_option('agv_saga_cui'),
                    'gestiune' => get_option('agv_saga_gestiune'),
                    'serie'    => get_option('agv_saga_serie'),
                    'livrare'  => get_option('agv_saga_livrare'),
                    'reducere' => get_option('agv_saga_reducere')
                ],
                'data'     => $order->data,
                'invoice'  => array_merge($order->meta_data, get_option('wpo_wcpdf_settings_general')),
                'products' => $productsArray
            ];
        }

        $agvSagaApi = new agvSagaApi;
        $export     = $agvSagaApi->call('call', $data);
        
        return $export['message'];
    }

    public function stripProducts(array $products) {
        $return = [];
        
        foreach ($products as $product) {
            $return[] = $product->data;
        }

        return $return;
    }

    /**
     * Generate random key for Shop.
     *
     * @param int $length
     * @return void
     */
    public function generateKey($length)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
