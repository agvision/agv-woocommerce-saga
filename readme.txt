=== AGV WooCommerce Sagas ===
Contributors: 
Tags: woocommerce, saga, accounting, export
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==

AGV WooCommerce Saga allows you to easily export your WooCommerce orders into the Saga accounting software.

== Installation ==

This section describes how to install the plugin and get it working.

1) Descărcați plugin-ul.

2) În panoul de administrare WordPress accesați pagina Plugins >> Add New și uploadați fișierul descărcat la pasul 1. După aceea activați plugin-ul.

3) Accesați tab-ul WooCommerce >> Settings >> Products >> AGV Saga și completați toate câmpurile exact așa cum sunt configurate în contabilitatea Saga Soft.

4) Pentru a exporta comenzi accesați pagina WooCommerce >> Orders, selectați comenzile ce vă interesează, folosiți dropdown-ul de sus (Bulk Actions) pentru a selecta Export Saga iar după aceea apăsați butonul Apply.

5) În Saga, accesați meniul Diverse >> Import date și importați fișierul descărcat la pasul 4 în tab-ul Import de date din fișiere generate XML.

6) Validați comenzile importate.

== Changelog ==
